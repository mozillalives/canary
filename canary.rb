
require 'net/http'
require 'net/smtp'
require 'cinch'
require 'yaml'

$CONFIG = YAML.load_file("config.yml")

class CanaryPlugin
  include Cinch::Plugin

  # listen_to :connect, method: :on_connect
  # listen_to :online,  method: :on_online
  # # listen_to :unaway,  method: :on_online
  # listen_to :offline, method: :on_offline
  # # listen_to :leaving, method: :on_leaving
  # # listen_to :away, method: :on_offline
  
  # def on_connect(m)
  #   User('pmanser').monitor
  #   User('pmanseri').monitor
  #   # @users_online = []
  #   # User($CONFIG['default_notify']).monitor
  #   # $CONFIG['sites'].each { |d| 
  #   #   for m in d['maintainers']
  #   #     User(m).monitor
  #   #     @bot.loggers.debug "monitoring user #{m}"
  #   #     @users_online << m
  #   #   end
  #   # }
  # end
  # 
  # def on_online(m, user)
  #   @bot.loggers.debug "user #{user} is now online"
  # end
  # 
  # def on_offline(m, user)
  #   @bot.loggers.debug "user #{user} is now offline"    
  # end
  
  # def on_leaving(m, user)
  #   @bot.loggers.debug "user #{user} is now offline"
  # end
  
  listen_to :"401", method: :missing_user
  def on_missing_user(m)
    @bot.loggers.debug "-------------------------------"
    @bot.loggers.debug "hi there"
    @bot.loggers.debug "-------------------------------"
  end
  
  # check every 7 minutes
  # timer 420, method: :request_main_pages
  timer 20, method: :request_main_pages
  def request_main_pages

    $CONFIG['sites'].each { |d| 
      if Net::HTTP.get_response(d['domain'], "/").code.to_i >= 400
        notify = d['maintainers'] || []
        notify.push($CONFIG['default_notify'])
        notified = false

        for n in notify
          # TODO on load, monitor all the possible users so this information is accurate
          u = User(n)
  
          # # @bot.loggers.debug u
          # @bot.loggers.debug "#{u.methods}"
          # @bot.loggers.debug "#{u.online}"
          # @bot.loggers.debug "#{u.in_whois}"
          # @bot.loggers.debug "#{u.synced?}"
          @bot.loggers.debug "-------------------------------"
          @bot.loggers.debug "trying #{n}"
          # if u.online and u.in_whois
          # if u.in_whois
            @bot.loggers.debug "found #{n}, sending message"
            u.send("#{d['name']} is down")
            # @bot.loggers.debug m.methods
            @bot.loggers.debug "message sent to #{n}"
            notified = true
            break
          # end
          @bot.loggers.debug "-------------------------------"
        end
        
#         email = $CONFIG['default_email']
#         # TODO allow overriding 
#         
#         if not notified and email
#           message = <<MESSAGE_END
#         To: #{d['email']}
#         #{d['name']} is down
# MESSAGE_END
# 
#           Net::SMTP.start($CONFIG['smtp_host']) do |smtp|
#             smtp.send_message message, $CONFIG['from_email'], d['email']
#           end
#         end
      end
    }
        
  end
end

bot = Cinch::Bot.new do
    configure do |c|
        c.nick = $CONFIG["nick"]
        c.server = $CONFIG["server"]
        c.user = $CONFIG["user"]
        c.ssl.use = true
        c.password = $CONFIG["password"]
        c.plugins.plugins = [CanaryPlugin]
        c.channels = $CONFIG["channels"]
    end
    
    # on :"401" do |m|
    #   @bot.loggers.debug "-------------------------------"
    #   @bot.loggers.debug m
    #   @bot.loggers.debug "-------------------------------"
    # end
end

bot.start
