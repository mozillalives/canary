# 
# To install fabric on ubuntu/debian, first get
#   sudo apt-get install python-setuptools
# Then run 
#   sudo easy_install fabric
# 

import os
from fabric.api import task, run, cd, sudo, settings
from fabric.contrib.console import confirm
from fabric.contrib.files import exists
from fabric.contrib.project import rsync_project

@task
def deploy():
    """attempts to do a full deployment of the bot to an ubuntu server"""
    if not exists("/home/canary"):
        # TODO make this a user that cannot login
        sudo("adduser canary")
    rsync_project(remote_dir="/tmp/canary/", local_dir="./", exclude=[".git", "fabfile.py*", "*.sample"], delete=True)
    sudo("cp -R /tmp/canary/* /home/canary")
    if not exists("/etc/init/canary.conf"):
        with cd("/etc/init"):
            sudo("ln -s /home/canary/canary.conf canary.conf")
            sudo("initctl reload-configuration")
    with settings(warn_only=True):
        sudo("service canary stop")
    sudo("service canary start")
    sudo("service canary status")
    
@task
def service(cmd='status'):
    """expects 'start', 'stop' or 'status'. defaults to 'status'"""
    sudo("service canary %s" % cmd)